### What Is The Thickness of The Center Flavor (Strawberry?) in Neapolitan Ice Cream?

I was eating Neapolitan ice cream from a large bucket and wondered this question... 
One could also determine the thickness of the chocolate and vanilla flavors and other results. 
For fareness-sake I assumed equal amounts of each flavor.

![Scoop](NeapolitanIcecreamPhoto.jpg)

I decided to solve it via Monte Carlo method. 
I solved the original question using one method, and secondary inquiries using another method. 
For both methods I generated _ntotal_ = 100000 points uniformly over a disk of unitary radius center at (0,0). 

### Method 1

Here I employed a "[binary search algorithm](https://en.wikipedia.org/wiki/Binary_search_algorithm)". 
I don't really know what it's called but I can explain what I did:
By exploiting equatorial symmetry parellel with the flavor interfaces that run -/+ y, 
and assuming uniform distribution of points, I only needed to examime the points in positive x 
(I really only needed the points in positive x AND y, but alas).

Since I am only looking at positive-x points, I only needed to check if the number of "vanilla" points is 1/3 of the total points ("vanilla" here are the points in the right side of the bucket). .
If this checks out, and because "chocolate" is just a mirror of vanilla (the points on the left side of the bucket), 
the remaining 1/3 should be "strawberry" (the points in the middle of the bucket).. 
But how does one count the number of vanilla points in some unique region?

1. Cut region [0,R=1] in half.
2. Center of that region is _lx_ = 0.5.
3. Count points greater than _lx_; let that number be _nvan_.
  * If _nvan_/_ntotal_ is grater than 1/3, increase _lx_: new _lx_ = 0.75 is half of space that was greater than _lx_ 
(_lx_old_ = 0.5 and 1.). 
  * If _nvan_/_ntotal_ is less than 1/3, decrease _lx_: new _lx_ = 0.25 is half of space that was lesser than _lx_
(0. and _lx_old_ = 0.5).
4. This is repeated for some number of bisections; the greater this number the greater the accuracy 
(precision is determined by _ntotal_).

When an adequate _lx_ is found, our result is found: the thickness of strawberry is 2 _lx_. 
The thickness of vanilla and chocolate are R-_lx_. 
All points are also given a label to plot: points with an x value greater than _lx_ are
labelled vanilla, points with an x value less than -_lx_ are labelled chocolate, 
and remaining points are labelled strawberry. This bucket of Neapolitan ice cream is visualized below.

![Colored](NeapolitanAreas.png)

If one was producing Neapolitan ice cream in large buckets, squirting a stripe of strawberry with a 
thickness of half the radius would suffice. This produced an accurate result for a traditional 
three-flavor ice cream, but what about 36 flavors...?



### Method 2

Instead of a binary search as above, I found the flavor interfaces by stepping through some number of 
x values starting at 0 and increasing towards R. The smaller the stepsize the more accurate this method becomes.
When the number of points enveloped by [0,interface) was equal to (or greater than) what it should be for _nflavors_, 
a list was appended, and a line was created and saved, for later:
```python
...
# If (nflavors == odd):
  if (nflavors % 2 == 1):
    q = 1
# If (nflavors == even):
  if (nflavors % 2 == 0):
    q = 0
    ROOT.TLine().DrawLine(0.,R,0.,-R) # Center line

  if npoints/n >= (2*flavorn-q)/nflavors:
    lines.append( (nflavors, step, sqrt(R**2-step**2), npoints, npoints/n, pi*R**2/nflavors) )
    # == (graph, x, y, npoints, %points, area)

   ROOT.TLine().DrawLine(step,sqrt(R**2-step**2), step,-sqrt(R**2-step**2))
   ROOT.TLine().DrawLine(-step,sqrt(R**2-step**2), -step,-sqrt(R**2-step**2))
...
```


I didn't want to generate many different colors so I left the ice cream points out and only plotted the interfaces.

![Grid](NeapolitanAreas_f=1-9.png)

After generating the below plot I noticed it resembled a rectangular hyperbola, and indeed I was correct: 
that is a perfect fit.

![Area vs nFlavors](NeapolitanAreas_R=1.0_f=1-36_avsf.png)

I guess this was a fancy way to say Area_1flavor = pi R^2 / _nflavors_. That was a big "duh" moment.

Coming back to the original question: what is the thickness of the center flavor? 
There is only a center flavor for when there is an odd-number of flavors. 
I also calculated the thickness of the edge flavor for all number of flavors.

![Thicknesses vs nFlavors](NeapolitanAreas_R=1.0_f=1-36_thvsf.png)

Please excuse whatever is happening at (0,0).
