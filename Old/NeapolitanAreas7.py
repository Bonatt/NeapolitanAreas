import ROOT
import numpy as np
import random
#import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
def sin(x):
  return np.sin(x)
def cos(x):
  return np.cos(x)
# Redefine pi to something shorter
pi = np.pi

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();
print ''





### Neapolitan ice cream: what's the width of each of the three flavors to achieve equal volumes?

'''
# Unit circle centered at (0,0).
R = 1.                                                          # Radius of circle
x0 = 0.
y0 = 0.

# Generate rando point locations within circle bounds.
n = 100000                                                        # number of points
print n, 'ice cream points'
r = np.random.uniform(low=0., high=R, size=n)                   # radius of point
theta = np.random.uniform(low=0., high=2.*pi, size=n)        # angle of point

# Location of points in rectangular coords.
x = x0 + sqrt(r)*cos(theta)
y = y0 + sqrt(r)*sin(theta)




# Greater lx means greater Strawberry volume. I just iterated this by hand.
#lx = 0.265#15
#ly = sqrt(R-lx**2)


### Binary Search 
# Only need to check if nvan is 1/3 of total points; no need to create all and compare.
# Cut space (0,R=1) in half. Center of that space is lx=0.5. Count points greater than lx. That number is nvan.
# If nvan/ntotal is greater than 0.33, move lx towards right: new lx=0.75 is half of space that was to the right 
# of lx: (lx_old=0.5,1).
# If less than, new lx=0.25 is half of to the left: (0,lx_old=0.5)... etc.

nflavors = 3
chops = 10

b1 = 0.
b2 = R
for chop in range(chops):

  lx = abs(b2+b1)/2 
  nvan = sum(1. for i in x if i > lx)

  #print 'b1 = '+str(b1), '\tlx = '+str(lx), '\tb2 = '+str(b2), '\tnvan/n = '+str(nvan/n)

  if nvan/n > 1./nflavors:       
    b1 = lx
  if nvan/n < 1./nflavors:
    b2 = lx

ly = sqrt(R-lx**2)




# Initialize graphs
gChoc = ROOT.TGraph()
gStr = ROOT.TGraph()
gVan = ROOT.TGraph()

# Segregate points into flavors depending position
for xx,yy in zip(x,y):
  if xx < -lx:
    gChoc.SetPoint(gChoc.GetN(), xx,yy)
  elif xx > lx:
    gVan.SetPoint(gVan.GetN(), xx,yy)
  else:
    gStr.SetPoint(gStr.GetN(), xx,yy)

nChoc = gChoc.GetN()
nStr = gStr.GetN()
nVan = gVan.GetN()
nTotal = float(nChoc+nStr+nVan)

print 'Chocolate volume:  ', str(nChoc/nTotal*100)+'% with an equatorial thickness of =', R-lx
print 'Strawberry volume: ', str(nStr/nTotal*100)+'% with an equatorial thickness of =', 2*lx
print 'Vanilla volume:    ', str(nVan/nTotal*100)+'% with an equatorial thickness of =', R-lx


### Plot rando points, colored like flavor
c = ROOT.TCanvas('c', 'c', 720, 720)
gChoc.GetXaxis().SetLimits(-R, R)
gChoc.GetYaxis().SetRangeUser(-R, R)
gChoc.SetTitle('Equatorial Thickness of Strawberry = '+str(2*lx)+' R (n = '+str(n)+');;')
gChoc.SetMarkerStyle(ROOT.kFullDotLarge)
gChoc.SetMarkerColor(ROOT.kOrange-7)
gChoc.SetMarkerSize(0.3)
gChoc.Draw('ap')

gStr.SetMarkerStyle(ROOT.kFullDotLarge)
gStr.SetMarkerColor(ROOT.kPink+1)
gStr.SetMarkerSize(0.3)
gStr.Draw('p same')

gVan.SetMarkerStyle(ROOT.kFullDotLarge)
gVan.SetMarkerColor(ROOT.kYellow-10)
gVan.SetMarkerSize(0.3)
gVan.Draw('p same')

# Draw flavor division lines
line1 = ROOT.TLine(-lx,ly,-lx,-ly)
line1.Draw()
line2 = ROOT.TLine(lx,ly,lx,-ly)
line2.Draw()
# Draw circle/circumference
circum = ROOT.TEllipse(x0,y0,R,R)
circum.SetFillStyle(0)
circum.Draw()

c.SaveAs('NeapolitanAreas.png')
'''































### Now try generalizing it all for n flavors again...
# Want to fill half circle with dots. 
# Move 1st/nflavors line from 0 to R, keeping track of dots to the left of it
# When dots left of it is 1/n, stop. Continue from here for 2nd/n line.
# --> When n = odd,  1st line must envelope 1/(2*n), rest envelope 1/n
# --> When n = even, 1st line must envelope 1/n, rest envelope 1/n too.


# Unit circle centered at (0,0).
R = 1.                                                          # Radius of circle
x0 = 0.
y0 = 0.

# Generate rando point locations within circle bounds.
n = 100000                                                        # number of points
print n, 'ice cream points'
r = np.random.uniform(low=0., high=R, size=n)                   # radius of point
theta = np.random.uniform(low=0., high=2.*pi, size=n)        # angle of point

# Location of points in rectangular coords. Make all x positive to create half circle in positive x
x = abs(x0 + sqrt(r)*cos(theta))
y = y0 + sqrt(r)*sin(theta)



print ''
print 'Now try generalizing it all for n flavors.'

nflavors = 5

lines = []
stepsize = R/100.


### Loop through steps, marking/graphing appropriately
flavorn = 1.
for step in np.arange(stepsize,R+stepsize, stepsize):
  npoints = sum(1. for i in x if i <= step)
  #print 'Step:x = '+format(step, '.2f'), 'of', format(R, '.2f'), '    |     Points:', int(npoints), 'of', n

  # If (nflavors == odd):
  if (nflavors % 2 == 1): q = 1
  # If (nflavors == even):
  if (nflavors % 2 == 0): q = 0
  
  if npoints/n >= (2*flavorn-q)/nflavors:   
    #print '  \'--> Line',int(flavorn),'at x =',format(step,'.2f'),'envelopes',str(flavorn/nflavors*100)+'%'
    lines.append( (step, sqrt(R-step**2), npoints, npoints/n) ) # (x, y, npoints, %points)
    flavorn+=1


# Percent of points between bounds a and b. For terminal checking
# a=0.; b=0.5; (sum(1. for i in X if i <= b) - sum(1. for i in X if i <= a))/n


### Plot rando points
c2 = ROOT.TCanvas('c2', 'f='+str(nflavors), 720, 720)
gN = ROOT.TGraph()
for xx,yy in zip(x,y):
  gN.SetPoint(gN.GetN(), xx,yy)
gN.GetXaxis().SetLimits(-R, R)
gN.GetYaxis().SetRangeUser(-R, R)
gN.SetTitle('f='+str(nflavors)+';;')
gN.SetMarkerColor(ROOT.kWhite)
gN.Draw('ap')

# Draw flavor division lines
line = ROOT.TLine()
for l in lines:
  line.DrawLine(l[0],l[1], l[0],-l[1])
  line.DrawLine(-l[0],l[1], -l[0],-l[1])
  if nflavors % 2 == 0:
    line.DrawLine(0.,R,0.,-R)

# Draw circle/circumference
circum = ROOT.TEllipse(x0,y0,R,R)
circum.SetFillStyle(0)
circum.Draw()

c2.SaveAs('NeapolitanAreas_f='+str(nflavors)+'.png')











### TODO:
'''
1. Plot flavors 1-9 circles on grid.
2. Plot area1, area2, etc as function of nflavors on grid
'''



