import ROOT
import numpy as np
import random
#import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
def sin(x):
  return np.sin(x)
def cos(x):
  return np.cos(x)
# Redefine pi to something shorter
pi = np.pi

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();
print ''





### Neopolitan ice cream: what's the width of each of the three flavors to achieve equal volumes?


# Unit circle centered at (0,0).
R = 1.                                                          # Radius of circle
x0 = 0.
y0 = 0.

# Generate rando point locations within circle bounds.
n = 10000#00                                                        # number of points
print n, 'ice cream points'
r = np.random.uniform(low=0., high=R, size=n)                   # radius of point
theta = np.random.uniform(low=0., high=2.*pi, size=n)        # angle of point

# Location of points in rectangular coords.
x = x0 + sqrt(r)*cos(theta)
y = y0 + sqrt(r)*sin(theta)





# Greater lx means greater Strawberry volume. I just iterated this by hand. Solve with code. Then for N flavors.
lx = 0.265#15
ly = sqrt(R-lx**2)

'''
Choc = []
Str = []
Van = []

# Segregate points into flavors depending position
for xx,yy in zip(x,y):
  if xx < -lx: Choc.append((xx,yy))
  elif xx > lx: Van.append((xx,yy))
  else: Str.append((xx,yy))
'''

### Only need to check if nChoc, e.g., is 1/3 of total points; no need to create all and compare.
'''
i = 0
for xx in x:
  if xx < -lx: i+=1
print 'i/n =', i/float(n)
'''
### Do Binomial search.
### Or five lx's, e.g., find which ever is closest to 33%, make new smaller window around that, do five more lx's within it. Repeat.

### Binary Search
nflavors = 3
chops = 40 # number of times to chop and pick best half

b1 = 0 # bound 1,2 of test area. Chop is center of bounds.
b2 = R
#lx = (b2-b1)/2 # initial point to chop
'''
nvan = 0.
for xx in x:
  if xx > lx: nvan+=1.
'''
'''
#lx2 = (b2-b1)/2
for chop in range(chops):
  lx = (b2-b1)/2 # initial point to chop
  nvan = 0.
  for xx in x:
    if xx > lx: nvan+=1.
  if nvan/n > 1./nflavors: 
    b1 = lx 
    #b2 = lx2
    #lx = (lx+b2)/2. #lx + lx/2.
  #else: 
  if nvan/n < 1./nflavors:
    b2 = lx
    #b1 = lx2
    #lx = (lx+b1)/2. #lx - lx/2.
  lx2 = lx
  print 'nvan/n = '+str(nvan/n), '\tlx = '+str(lx) #, '\tb1='+str(b1), '\tb2='+str(b2)
'''
### Why does the above hone in on nvan/n = 0.3939 lx = 0.166666666667? Want lx ~= 0.265. Try again.

### Binary Search 2
nflavors = 3
chops = 20

b1 = 0.
b2 = R
lx = (b2-b1)/2 
#lxOld = lx
for chop in range(chops):

  lx = abs(b2+b1)/2 

  nvan = sum(1. for i in x if i > lx) #len([i for i in x if i > lx])

  print 'b1 = '+str(b1), '\tlx = '+str(lx), '\tb2 = '+str(b2), '\tnvan/n = '+str(nvan/n)

  if nvan/n > 1./nflavors:       
    b1 = lx
    #lx2 = b2
    #b2 = lxOld
  if nvan/n < 1./nflavors:
    b2 = lx
    #lx2 = b1
    #b1 = lxOld

  #lxOld = lx
  #lx = (b2-b1)/2 


### Binary psuedocode:
'''
nflavors = 3
chops = 10

# "A" = section to be chopped in half
# "A1", "A2" = sections formed by bifurcating A
# "b1_A"  = lower bound of A
# "b2_A"  = upper bound of A
# "b1_A1" = lower bound of A1
# "b2_A1" = upper bound of A1
# "b1_A2" = lower bound of A2
# "b2_A2" = upper bound of A2
# lx = line x locations that bifurcates A

# First chop bounds
b1_A = 0.
b2_A = R
for chop in range(chops):
  lx = abs(b2_A-b1_A)/2

  nvan = sum(1. for i in x if i > lx)

  


'''

#### Above, lx is referencing itself, thus asomptically something itself. Variable name change my fix?




#lx = 0.5
### Five search
'''
### From https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return array[idx]

LX = 0
nflavors = 3.
chops = 20
for chop in range(chops):
  lxs = [r*R/5 for r in range(1,6)]
  for lx in lxs:
    i = 0
    for xx in x:
      if xx < -lx: i+=1
  

    
lx = LX
'''


ly = sqrt(R-lx**2)



# Initialize graphs
gChoc = ROOT.TGraph()
gStr = ROOT.TGraph()
gVan = ROOT.TGraph()

# Segregate points into flavors depending position
for xx,yy in zip(x,y):
  if xx < -lx:
    gChoc.SetPoint(gChoc.GetN(), xx,yy)
  elif xx > lx:
    gVan.SetPoint(gVan.GetN(), xx,yy)
  else:
    gStr.SetPoint(gStr.GetN(), xx,yy)

nChoc = gChoc.GetN()
nStr = gStr.GetN()
nVan = gVan.GetN()
nTotal = float(nChoc+nStr+nVan)

print 'Chocolate volume:  ', str(nChoc/nTotal*100)+'% with an equatorial thickness of =', R-lx
print 'Strawberry volume: ', str(nStr/nTotal*100)+'% with an equatorial thickness of =', 2*lx
print 'Vanilla volume:    ', str(nVan/nTotal*100)+'% with an equatorial thickness of =', R-lx





### Plot rando points, colored like flavor
c = ROOT.TCanvas('c', 'c', 720, 720)
gChoc.GetXaxis().SetLimits(-R, R)
gChoc.GetYaxis().SetRangeUser(-R, R)
gChoc.SetTitle('Equatorial Thickness of Strawberry = '+str(2*lx)+' au (n = '+str(n)+');;')
gChoc.SetMarkerStyle(ROOT.kFullDotLarge)
gChoc.SetMarkerColor(ROOT.kOrange-7)
gChoc.SetMarkerSize(0.3)
gChoc.Draw('ap')

gStr.SetMarkerStyle(ROOT.kFullDotLarge)
gStr.SetMarkerColor(ROOT.kPink+1)
gStr.SetMarkerSize(0.3)
gStr.Draw('p same')

gVan.SetMarkerStyle(ROOT.kFullDotLarge)
gVan.SetMarkerColor(ROOT.kYellow-10)
gVan.SetMarkerSize(0.3)
gVan.Draw('p same')

# Draw flavor division lines
line1 = ROOT.TLine(-lx,ly,-lx,-ly)
line1.Draw()
line2 = ROOT.TLine(lx,ly,lx,-ly)
line2.Draw()
# Draw circle/circumference
circum = ROOT.TEllipse(x0,y0,R,R)
circum.SetFillStyle(0)
circum.Draw()

c.SaveAs('NeopolitanVolumes2.png')
